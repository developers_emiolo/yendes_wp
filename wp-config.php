<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa user o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações
// com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'yendes');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'root');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', '');

/** Nome do host do MySQL */
define('DB_HOST', 'localhost');

/** Charset do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8mb4');

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para desvalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'B#T1<YT/T@HfO!g,,YId-<:$yXp%9|.4qRv*W=*-9-Ee4*] BBmodE`:XV{>K}RZ');
define('SECURE_AUTH_KEY',  't{}RtPTXH#Rv4(ki$*C+BPq(ui E!Bm3@9<4:],%uP(ELF>6F$}^q!z>7g_J5{}*');
define('LOGGED_IN_KEY',    'Abq#|=_qGULzfnQ[<dZ[N]35ii[qZr:$8dIo.;n*K0EmHU $eo1.8-{@;GwK|Ek_');
define('NONCE_KEY',        ')$kp|:D)|K;vyDIB<K:ZOQ9E_}Gm(TB7W}rrF@z;}_#f-rz{4g9%gWDpwi3N=ikO');
define('AUTH_SALT',        'h[Ay#x!)NNL#+}9fszo,Q.[LWn*Ai-N3qc+9NEGQ<zy_/H^fe}nXq}%]W>N5);LR');
define('SECURE_AUTH_SALT', 'bIyK~]2`0)GETTxf>7iYcveJaTYz`MWq0?oZ5_$A.C2.mGVFVdASRo}j.WElMQbr');
define('LOGGED_IN_SALT',   '~9$JTE6R%4`s@NeO@N4H*R#,8>A]FgO9-HeeLZ*h]EjP>[h/DCrp+!jomCV%BIW3');
define('NONCE_SALT',       'O$VD;u-n8cU29s5|}wdC4jzy$#wDEY0Wf*WI^}qgfD<?c<Y(jB<dSEy3Ktx{@4uQ');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * para cada um um único prefixo. Somente números, letras e sublinhados!
 */
$table_prefix  = 'wp_';

/**
 * Para desenvolvedores: Modo debugging WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
